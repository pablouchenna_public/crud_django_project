# -*- coding: utf-8 -*-
'''
@author: pablo
'''

from setuptools import setup, find_packages


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='post_codes_UK',
      version='1.0.0',
      description='''Write a library that supports validating and formatting post codes for UK. 
  The details of which post codes are valid and which are the parts they consist of can be found at 
  https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting''',
      long_description=readme(),
      author='Pablo Uchenna',
      author_email='pablouchennaonuohacid@gmail.com',
      license='LICENSE.txt',
      classifiers=[
          'Development Status :: 5 - Production/Stable',
          'Intended Audience :: Scurri',
          'Topic :: Programming Exercise',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3.9',
      ],
      include_package_data=True,
      exclude_package_data={"": ["*.log", '.project', '.pydevproject']},
      packages=find_packages(where = 'post_codes_UK_django_project',
                             exclude=['contrib', 'docs', 'tests*']),
#       package_data={"": ["db.sqlite3"]},
      data_files=[('', ['post_codes_UK_django_project/db.sqlite3'])],
      package_dir = {'': 'post_codes_UK_django_project'},
      py_modules = ['manage'],
      install_requires=['asgiref==3.3.4',
                        'django-filter==2.4.0',
                        'django==3.2',
                        'djangorestframework==3.12.4',
                        'markdown==3.3.4',
                        'pytz==2021.1',
                        'sqlparse==0.4.1',
                        ],
      python_requires='>=3.9',
#       test_suite='tests',
#       entry_points={
#           'console_scripts': ['post_codes_UK_runserver=manage:runserver',
#                               'post_codes_UK_test=manage:test'],
#       },
      zip_safe=False)
