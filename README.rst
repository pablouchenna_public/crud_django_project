Media Files CRUD Example
-------------



Installation
------------

It requires python >= 3.9

Go to the installation directory of you choice.

To work in a virtual environment you may use:
::

  $ pip install --user pipenv
  $ pipenv --python 3.9
  $ pipenv install
  $ pipenv shell  # activates the virtual environment


Tests
-----
::

  $ cd pond5_CRUD_project
  $ python manage.py test -v 3 pond5_CRUD_app/tests


Use
----
::

  $ cd pond5_CRUD_project
  $ python manage.py runserver

  # It will run at http://127.0.0.1:8000/


::

  ## Small service that supports CRUD API

  POST 201
    POST /mediafile/
    # HTTP 201 Created
    Allow: GET, POST, HEAD, OPTIONS
    Content-Type: application/json
    Vary: Accept
    {
        "id": 5,
        "file_name": "file_name_4",
        "media_type": "wav",
        "created_at": "2021-05-12T17:57:23.763652Z",
        "updated_at": "2021-05-12T17:57:23.763703Z"
    }
  POST 400
    POST /mediafile/
    HTTP 400 Bad Request
    Allow: GET, POST, HEAD, OPTIONS
    Content-Type: application/json
    Vary: Accept
    {
        "file_name": [
            "media file with this file name already exists."
        ],
        "media_type": [
            "This field may not be blank."
        ]
    }

  GET 200
      GET /mediafile/2/
      HTTP 200 OK
      Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
      Content-Type: application/json
      Vary: Accept
      {
          "id": 2,
          "file_name": "f2",
          "media_type": "avi",
          "created_at": "2021-05-06T14:15:14.478332Z",
          "updated_at": "2021-05-06T14:15:14.478472Z"
      }
  GET 404
      GET /mediafile/1/
      HTTP 404 Not Found
      Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
      Content-Type: application/json
      Vary: Accept
      {
          "detail": "Not found."
      }

  PUT 200
      PUT /mediafile/2/
      HTTP 200 OK
      Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
      Content-Type: application/json
      Vary: Accept
      {
          "id": 2,
          "file_name": "f2",
          "media_type": "mp3",
          "created_at": "2021-05-06T14:15:14.478332Z",
          "updated_at": "2021-05-12T21:27:04.838716Z"
      }
  PUT 404
      PUT /mediafile/211/
      HTTP 404 Not Found
      Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
      Content-Type: application/json
      Vary: Accept
      {
          "detail": "Not found."
      }

  DELETE 204
      DELETE /mediafile/2/
      HTTP 204 No Content
      Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
      Content-Type: application/json
      Vary: Accept
  DELETE 404
      DELETE /mediafile/211/
      HTTP 404 Not Found
      Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
      Content-Type: application/json
      Vary: Accept
      {
          "detail": "Not found."


###Protected from **SQL injection**.

Media Types could be limited to a list like [mp3, avi, ...].

Swagger or similar could be used for the documentation.

Provides `list`, `create`, `retrieve`, `update` and `destroy` actions.
***
# Request methods (VERBS)
> ### DELETE
> * /mediafile/<id\>/ delete the element with id=<id\>.
> ### GET
> * /mediafile/<id\>/ get the element with id=<id\>.
> * Example: [/mediafile/1/](/mediafile/1/)
> ### POST
> * /mediafile/ creates a new element with the data provided (file_name and media_type).
> ### PUT
> * /mediafile/<id\>/ update the element with id=<id\>.
> ### EXTRA:
> By adding a ".json" or ".api" to "mediafile" on the url,
> the API will return a json o a nice html page.
>
> Example:
> > * [/mediafile.json/](/mediafile.json/) list all elements using json
> > * [/mediafile.api/](/mediafile.api/) list all elements using a nice html page

## curl examples:
    POST
    curl --header "Content-Type: application/json" \
--request POST \
--data '{"file_name":"my_file_1.mp3","media_type":"mp3"}' \
http://127.0.0.1:8000/mediafile/
    curl --header "Content-Type: application/json" \
--request POST \
--data '{"file_name":"my_file_2.mp3","media_type":"mp3"}' \
http://127.0.0.1:8000/mediafile/
    
    GET
    curl --header "Content-Type: application/json" --request GET http://127.0.0.1:8000/mediafile/
    curl --header "Content-Type: application/json" --request GET http://127.0.0.1:8000/mediafile/1/

    PUT
    curl --header "Content-Type: application/json" \
--request PUT \
--data '{"file_name":"my_file_1_updated.mp3","media_type":"mp3"}' \
http://127.0.0.1:8000/mediafile/1/
    
    DELETE
    curl --header "Content-Type: application/json" --request DELETE http://127.0.0.1:8000/mediafile/1/


The DELETE, PUT and UPDATE methods could be protected with **user/password authentication**.
**CSRF protection** (Cross Site Request Forgery) could be implemented,
to minimize the risk of using your credentials where you are logged.
**CORS** (Cross-Origin Resource Sharing) could be enabled, as browser's same-origin
policy may block accessing a resource from a different origin.

### JSON **example** for POST or UPDATE
>     {
>         "file_name": "my_file.mp3",
>         "media_type": "mp3"
>     }

### JSON **example** for GET
>     {
>         "id": 1,
>         "file_name": "my_file.mp3",
>         "media_type": "mp3",
>         "created_at": "2021-05-06T14:13:20.405434Z",
>         "updated_at": "2021-05-06T14:13:20.406045Z"
>     }
***


Version
-------

BumpVersion can be used to change the version wherever it is needed


Issues
--------

*Just if you want to build a python package*
	For different reasons I had to install the following:
	::
	
	  sudo apt install libffi-dev -y
	  sudo apt-get install libssl-dev
	  sudo apt-get install zlib1g-dev
	  sudo apt install libsqlite3-dev
	
	And then recomplile python 3.9:
	::
	
	  make clean
	  ./configure
	  make
	  make test
	  sudo make altinstall


Comments
--------

Regards,
Pablo
