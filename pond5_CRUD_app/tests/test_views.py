'''
Created on 12 May 2021

@author: pablo
'''
from django.test import TestCase
from pond5_CRUD_app.views import MediaFileViewSet
from rest_framework.test import APIRequestFactory


class TestMediaFileViewSet(TestCase):
    '''
    This is a long time used and well tested CRUD API provider.
    For the purpose of this CRUD API no further tests are needed.

    Two tests are created to provide example:
        test_MediaFileViewSet_POST_create_201_created
        test_MediaFileViewSet_POST_create_400_bad_request

    Others:
        GET 200
            GET /mediafile/2/
            HTTP 200 OK
            Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
            Content-Type: application/json
            Vary: Accept
            {
                "id": 2,
                "file_name": "f2",
                "media_type": "avi",
                "created_at": "2021-05-06T14:15:14.478332Z",
                "updated_at": "2021-05-06T14:15:14.478472Z"
            }
        GET 404
            GET /mediafile/1/
            HTTP 404 Not Found
            Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
            Content-Type: application/json
            Vary: Accept
            {
                "detail": "Not found."
            }

        PUT 200
            PUT /mediafile/2/
            HTTP 200 OK
            Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
            Content-Type: application/json
            Vary: Accept
            {
                "id": 2,
                "file_name": "f2",
                "media_type": "mp3",
                "created_at": "2021-05-06T14:15:14.478332Z",
                "updated_at": "2021-05-12T21:27:04.838716Z"
            }
        PUT 404
            PUT /mediafile/211/
            HTTP 404 Not Found
            Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
            Content-Type: application/json
            Vary: Accept
            {
                "detail": "Not found."
            }

        DELETE 204
            DELETE /mediafile/2/
            HTTP 204 No Content
            Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
            Content-Type: application/json
            Vary: Accept
        DELETE 404
            DELETE /mediafile/211/
            HTTP 404 Not Found
            Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS
            Content-Type: application/json
            Vary: Accept
            {
                "detail": "Not found."
            }
    '''

    def setUp(self):
        self.factory = APIRequestFactory()
        self.base_url = '/mediafile/'
        self.data = {
            "file_name": "file_name_4",
            "media_type": "wav",
        }

    def tearDown(self):
        pass

    def test_MediaFileViewSet_POST_create_201_created(self):
        '''
        # HTTP 201 Created
        Allow: GET, POST, HEAD, OPTIONS
        Content-Type: application/json
        Vary: Accept
        {
            "id": 5,
            "file_name": "file_name_4",
            "media_type": "wav",
            "created_at": "2021-05-12T17:57:23.763652Z",
            "updated_at": "2021-05-12T17:57:23.763703Z"
        }
        '''
        request = self.factory.post(self.base_url, self.data, format='json')
        view = MediaFileViewSet.as_view({'post': 'create'})
        response = view(request)
        response.render()  # Cannot access `response.content` without this.
        self.assertEqual(response.status_code, 201)  # HTTP 201 Created
        self.assertDictContainsSubset({"file_name": "file_name_4",
                                       "media_type": "wav",
                                       },
                                      response.data)

    def test_MediaFileViewSet_POST_create_400_bad_request(self):
        '''
        HTTP 400 Bad Request
        Allow: GET, POST, HEAD, OPTIONS
        Content-Type: application/json
        Vary: Accept
        {
            "file_name": [
                "media file with this file name already exists."
            ],
            "media_type": [
                "This field may not be blank."
            ]
        }
        '''
        request = self.factory.post(self.base_url,
                                    {"file_name": "file_name_4"},
                                    format='json')
        view = MediaFileViewSet.as_view({'post': 'create'})
        response = view(request)
        response.render()  # Cannot access `response.content` without this.
        self.assertEqual(response.status_code, 400)  # HTTP 400 Bad Request
