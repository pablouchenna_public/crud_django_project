'''
Created on 12 May 2021

@author: pablo
'''
from django.core.exceptions import ValidationError
from django.test import TestCase

from pond5_CRUD_app.models import MediaFile


class MediaFileTests(TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_no_file_name(self):
        data = {
            "media_type": "wav",
        }
        media_file = MediaFile.objects.create(**data)
        with self.assertRaises(ValidationError):
            media_file.full_clean()

    def test_no_media_type(self):
        data = {
            "file_name": "file_name_4",
        }
        media_file = MediaFile.objects.create(**data)
        with self.assertRaises(ValidationError):
            media_file.full_clean()

    def test_creation_mandatory_fields(self):
        data = {
            "file_name": "file_name_4",
            "media_type": "wav",
        }
        media_file = MediaFile.objects.create(**data)
        media_file.full_clean()
        media_file_dict = vars(media_file)
        self.assertDictContainsSubset(data, media_file_dict)

    def test_creation_automatic_date_fields(self):
        data = {
            "file_name": "file_name_4",
            "media_type": "wav",
        }
        media_file = MediaFile.objects.create(**data)
        media_file.full_clean()
        media_file_dict = vars(media_file)
        self.assertTrue('id' in media_file_dict)
        self.assertTrue('created_at' in media_file_dict)
        self.assertTrue('updated_at' in media_file_dict)
