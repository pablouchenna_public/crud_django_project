from rest_framework import viewsets

from pond5_CRUD_app.models import MediaFile
from pond5_CRUD_app.serializers import MediaFileSerializer


class MediaFileViewSet(viewsets.ModelViewSet):
    """
###Protected from **SQL injection**.

Media Types could be limited to a list like [mp3, avi, ...].

Swagger or similar could be used for the documentation.

Provides `list`, `create`, `retrieve`, `update` and `destroy` actions.
***
# Request methods (VERBS)
> ### DELETE
> * /mediafile/<id\>/ delete the element with id=<id\>.
> ### GET
> * /mediafile/<id\>/ get the element with id=<id\>.
> * Example: [/mediafile/1/](/mediafile/1/)
> ### POST
> * /mediafile/ creates a new element with the data provided (file_name and media_type).
> ### PUT
> * /mediafile/<id\>/ update the element with id=<id\>.
> ### EXTRA:
> By adding a ".json" or ".api" to "mediafile" on the url,
> the API will return a json o a nice html page.
>
> Example:
> > * [/mediafile.json/](/mediafile.json/) list all elements using json
> > * [/mediafile.api/](/mediafile.api/) list all elements using a nice html page

## curl examples:
    POST
    curl --header "Content-Type: application/json" \
--request POST \
--data '{"file_name":"my_file_1.mp3","media_type":"mp3"}' \
http://127.0.0.1:8000/mediafile/
    curl --header "Content-Type: application/json" \
--request POST \
--data '{"file_name":"my_file_2.mp3","media_type":"mp3"}' \
http://127.0.0.1:8000/mediafile/
    
    GET
    curl --header "Content-Type: application/json" --request GET http://127.0.0.1:8000/mediafile/
    curl --header "Content-Type: application/json" --request GET http://127.0.0.1:8000/mediafile/1/

    PUT
    curl --header "Content-Type: application/json" \
--request PUT \
--data '{"file_name":"my_file_1_updated.mp3","media_type":"mp3"}' \
http://127.0.0.1:8000/mediafile/1/
    
    DELETE
    curl --header "Content-Type: application/json" --request DELETE http://127.0.0.1:8000/mediafile/1/


The DELETE, PUT and UPDATE methods could be protected with **user/password authentication**.
**CSRF protection** (Cross Site Request Forgery) could be implemented,
to minimize the risk of using your credentials where you are logged.
**CORS** (Cross-Origin Resource Sharing) could be enabled, as browser's same-origin
policy may block accessing a resource from a different origin.

### JSON **example** for POST or UPDATE
>     {
>         "file_name": "my_file.mp3",
>         "media_type": "mp3"
>     }

### JSON **example** for GET
>     {
>         "id": 1,
>         "file_name": "my_file.mp3",
>         "media_type": "mp3",
>         "created_at": "2021-05-06T14:13:20.405434Z",
>         "updated_at": "2021-05-06T14:13:20.406045Z"
>     }
***
    """
    queryset = MediaFile.objects.all()
    serializer_class = MediaFileSerializer
