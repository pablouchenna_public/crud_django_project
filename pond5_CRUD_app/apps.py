from django.apps import AppConfig


class Pond5CrudAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pond5_CRUD_app'
