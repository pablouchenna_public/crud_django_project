'''
Created on 4 May 2021

@author: pablo
'''
from rest_framework import serializers

from pond5_CRUD_app.models import MediaFile


class MediaFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = MediaFile
        fields = ['id',
                  'file_name',
                  'media_type',
                  'created_at',
                  'updated_at']
