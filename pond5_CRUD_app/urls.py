'''
Created on 6 May 2021

@author: pablo
'''
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from pond5_CRUD_app import views

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'mediafile', views.MediaFileViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
]
