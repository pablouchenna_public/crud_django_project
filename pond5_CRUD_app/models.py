from django.db import models

# MEDIA_TYPE_CHOICES = [('mp3', 'MPEG-1 Audio Layer III or MPEG-2 Audio Layer III'),
#                       ('mp4','MPEG-4 Part 14'),
#                       ('mpeg','Moving Picture Experts Group'),
#                       ]


class MediaFile(models.Model):
    '''
    Assumptions:

    The id field is auto-generated, it could be a random hash value to increase security.
    created_at and updated_at are auto-generated.
    file_name is unique.
    file_name and media_type are mandatory.

    The DELETE, PUT and UPDATE methods could be protected with **user/password authentication**.
    **CSRF protection** (Cross Site Request Forgery) could be implemented,
    to minimize the risk of using your credentials where you are logged.
    **CORS** (Cross-Origin Resource Sharing) could be enabled, as browser's same-origin
    policy may block accessing a resource from a different origin.
    '''
    created_at = models.DateTimeField(auto_now_add=True)  # auto modified on creation
    updated_at = models.DateTimeField(auto_now=True)  # auto modified on every update
    file_name = models.CharField(blank=False, unique=True, max_length=255)
    # For extra security we may check that the media_type matches the file type.
    media_type = models.CharField(blank=False, max_length=255)
#     media_type = models.CharField(choices=MEDIA_TYPE_CHOICES, default=MEDIA_TYPE_CHOICES[0][0], max_length=255)

    class Meta:
        ordering = ['file_name']
